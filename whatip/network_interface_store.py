from gi.repository import Gtk, Gio, GObject
from whatip.conf_keys import ConfKeys
from whatip.network_interface import (
    NetType,
    NetworkInterface,
)
from whatip.network_interface_listbox_row import NetworkInterfaceListboxRow


class NetworkInterfaceStore(Gtk.SortListModel):
    __refreshing = False

    def __init__(self, listbox: Gtk.ListBox, gsettings: Gio.Settings):
        self.gsettings = gsettings
        self.listbox = listbox

        self.filter = Gtk.CustomFilter()
        self.invalidate_filter()
        self.sorter = Gtk.CustomSorter()
        self.invalidate_sort()
        self.list_store = Gio.ListStore(item_type=NetworkInterface)
        self.filter_store = Gtk.FilterListModel(
            model=self.list_store, filter=self.filter
        )
        super().__init__(model=self.filter_store, sorter=self.sorter)

        self.listbox.bind_model(self, self.__create_listbox_row, None)

    @GObject.Property(type=bool, default=False)
    def refreshing(self) -> bool:  # type: ignore
        return self.__refreshing

    @refreshing.setter
    def refreshing(self, nval: bool):  # type: ignore
        self.__refreshing = nval

    def invalidate_sort(self, *_):
        self.sorter.set_sort_func(self.__sort_func)

    def invalidate_filter(self, *_):
        self.filter.set_filter_func(self.__filter_func)

    def __create_listbox_row(
        self, item: NetworkInterface, *_
    ) -> Gtk.ListBoxRow:
        row = NetworkInterfaceListboxRow(item)
        return row

    def __sort_func(self, net1: NetworkInterface, net2: NetworkInterface, *_):
        if net1.interface_type == NetType.INTERNET:
            return -1
        if net2.interface_type == NetType.INTERNET:
            return 1
        if net1.is_physical():
            return -1
        if net2.is_physical():
            return 1
        return -1 if net1.interface.lower() < net2.interface.lower() else 1

    def __filter_func(self, net: NetworkInterface) -> bool:
        return net.valid and net.interface_type != NetType.LOOPBACK

    def add(self, item: NetworkInterface):
        self.list_store.append(item)
        item.connect('notify::address', self.invalidate_sort)
        item.connect('notify::interface', self.invalidate_sort)
        item.connect('notify::valid', self.invalidate_filter)

    def populate(self):
        self.refreshing = True
        self.list_store.remove_all()
        for iface in NetworkInterface.get_all_interfaces():
            self.add(iface)
        if bool(
            self.gsettings.get_boolean(
                ConfKeys.ENABLE_THIRD_PARTIES
            )
        ):
            self.add(NetworkInterface(None, True))
        self.refreshing = False
