import requests
import json
from gi.repository import Gtk, GLib
from gettext import gettext as _
from threading import Thread
from whatip.network_ports import NetPort, PortType
from whatip.test_connection import is_online_sync


def __get_services():
    services = dict()
    lines = None
    with open('/etc/services', 'r') as fd:
        lines = fd.read().split('\n')
    for line in lines:
        if line == '' or line.startswith('#'):
            continue
        line_split = line.split()
        port, port_type, service = (None, None, None)
        if len(line_split) > 2:
            if '#' in line:
                line = line.split('#')[0].strip()
            while '/' not in line.split()[-1] or line.count('/') > 1:
                line = ' '.join(line.split()[:-1])
            if len(line.split()) > 2:
                port, port_type = line.split()[-1].split('/')
                service = ' '.join(line.split()[:-1])
        if port is None:
            service = line_split[0]
            port, port_type = line_split[1].split('/')
        if port not in services.keys():
            services[port] = dict()
        services[port][port_type] = service
        if (
            'tcp' in services[port].keys()
            and 'udp' in services[port].keys()
            and services[port]['tcp'] == services[port]['udp']
        ):
            services[port]['tcp/udp'] = services[port]['tcp']
    return services


SERVICES = __get_services()


@Gtk.Template(resource_path='/org/gabmus/whatip/ui/net_port_listbox_row.ui')
class PortListboxRow(Gtk.ListBoxRow):
    __gtype_name__ = 'PortListboxRow'
    mainbox = Gtk.Template.Child()
    tcp_badge = Gtk.Template.Child()
    udp_badge = Gtk.Template.Child()
    port_label = Gtk.Template.Child()
    service_label = Gtk.Template.Child()
    address_label = Gtk.Template.Child()
    status_icon = Gtk.Template.Child()
    test_btn = Gtk.Template.Child()

    def __init__(self, netport: NetPort):
        super().__init__()
        self.netport = netport

        port_types_str = ''
        if PortType.TCP in self.netport.types:  # type: ignore
            if PortType.UDP in self.netport.types:  # type: ignore
                port_types_str = 'tcp/udp'
            else:
                port_types_str = 'tcp'
        elif PortType.UDP in self.netport.types:  # type: ignore
            port_types_str = 'udp'

        for t, b in zip(
            [PortType.TCP, PortType.UDP], [self.tcp_badge, self.udp_badge]
        ):
            if t in self.netport.types:  # type: ignore
                b.set_visible(True)
        self.port_label.set_text(str(self.netport.port_num))

        service = SERVICES.get(str(self.netport.port_num), dict()).get(
            port_types_str, _('Unknown service')
        )
        process = (
            ''
            if self.netport.process is None
            else '\n'
            + _('Process: "{0}" (PID: {1})').format(
                self.netport.process.name, self.netport.process.pid
            )
        )

        self.service_label.set_text(service + process)
        self.address_label.set_text(
            '\n'.join(self.netport.addresses)  # type: ignore
        )

    def _test_port_async(self, retry=0):
        if not is_online_sync():
            GLib.idle_add(self.set_status, False)
        res = requests.get(f'https://ifconfig.co/port/{self.netport.port_num}')
        reachable = False
        try:
            reachable = json.loads(res.text)['reachable']
        except Exception:
            print(_('Error parsing json'))
            if retry < 3:
                print(_('Retrying...'))
                return self._test_port_async(retry=retry + 1)
        GLib.idle_add(self.set_status, reachable)

    def test_port(self):
        Thread(target=self._test_port_async, daemon=True).start()

    def set_status(self, reachable):
        if reachable:
            self.status_icon.set_from_icon_name(
                'emblem-ok-symbolic'
            )
            self.status_icon.set_tooltip_text(
                _('Port reachable')
            )
        else:
            self.status_icon.set_from_icon_name(
                'window-close-symbolic'
            )
            self.status_icon.set_tooltip_text(
                _('Port unreachable')
            )
        self.test_btn.set_sensitive(True)

    @Gtk.Template.Callback()
    def on_test_btn_clicked(self, btn):
        btn.set_sensitive(False)
        self.test_port()
