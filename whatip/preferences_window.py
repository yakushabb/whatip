from gettext import gettext as _
from typing import Optional
from gi.repository import Adw, Gio
from whatip.base_app import BaseWindow
from whatip.conf_keys import ConfKeys


def boundSwitchRow(
    title: str,
    gsettings: Gio.Settings,
    key: str,
    subtitle: Optional[str] = None,
) -> Adw.SwitchRow:
    row = Adw.SwitchRow(title=title, subtitle=subtitle)
    gsettings.bind(key, row, 'active', Gio.SettingsBindFlags.DEFAULT)
    return row


class PreferencesWindow(Adw.PreferencesWindow):
    def __init__(self, parent_win: BaseWindow):
        super().__init__(default_width=360, default_height=600)
        self.set_transient_for(parent_win)
        self.set_modal(True)

        self.general_page = Adw.PreferencesPage(
            title=_('General'), icon_name='preferences-other-symbolic'
        )
        self.general_group = Adw.PreferencesGroup(
            title=_('General preferences')
        )

        for row in [
            boundSwitchRow(title, parent_win.app.gsettings, key, subtitle)
            for title, subtitle, key in [
                (_('Dark mode'), None, ConfKeys.DARK_MODE),
                (
                    _('Enable third party services'),
                    None,
                    ConfKeys.ENABLE_THIRD_PARTIES,
                ),
            ]
        ]:
            self.general_group.add(row)
        self.general_page.add(self.general_group)

        self.add(self.general_page)
