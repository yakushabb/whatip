from typing import Iterable, Set
from gi.repository import GObject


class LanDevice(GObject.Object):
    __address: str
    __hostname: str
    __ports: Iterable[int]

    def __init__(self, address: str, hostname: str, ports: Set[int]):
        super().__init__()
        self.__address = address
        self.__hostname = hostname
        self.__ports = ports

    @GObject.Property(type=str)
    def address(self) -> str:
        return self.__address

    @GObject.Property(type=str)
    def hostname(self) -> str:
        return self.__hostname

    @GObject.Property
    def ports(self) -> Iterable[int]:
        return self.__ports

    @GObject.Property(type=bool, default=False)
    def has_http(self) -> bool:
        return 80 in self.__ports

    @GObject.Property(type=bool, default=False)
    def has_ftp(self) -> bool:
        return 21 in self.__ports

    def as_int(self) -> int:
        return int(self.address.replace('.', ''))
