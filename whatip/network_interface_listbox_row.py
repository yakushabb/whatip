from gi.repository import Gtk, GObject
from whatip.network_interface import NetType, NetworkInterface
from gettext import gettext as _
from whatip.clipboard_helper import clipboard_copy

NET_ICONS = {
    NetType.INTERNET: 'globe-symbolic',
    NetType.WIRED: 'network-wired-symbolic',
    NetType.WIRELESS: 'network-wireless-symbolic',
    NetType.MOBILE: 'phone-symbolic',
    NetType.VIRTUAL_BRIDGE: 'bridge-symbolic',
    NetType.TUNNEL: 'network-vpn-symbolic',
    NetType.LOOPBACK: 'media-playlist-repeat-symbolic',
    NetType.WIREGUARD: 'network-vpn-symbolic',
    NetType.DOCKER: 'docker-symbolic',
    NetType.UNKNOWN: 'dialog-question-symbolic'
}
NET_NAMES = {
    NetType.INTERNET: _('Public Internet'),
    NetType.WIRED: _('Wired'),
    NetType.WIRELESS: _('Wireless'),
    NetType.MOBILE: _('Mobile'),
    NetType.VIRTUAL_BRIDGE: _('Virtual bridge'),
    NetType.TUNNEL: _('Tunnel'),
    NetType.LOOPBACK: _('Loopback'),
    NetType.WIREGUARD: _('WireGuard'),
    NetType.DOCKER: _('Docker network'),
    NetType.UNKNOWN: _('Unknown')
}


@Gtk.Template(resource_path='/org/gabmus/whatip/ui/net_iface_listbox_row.ui')
class NetworkInterfaceListboxRow(Gtk.ListBoxRow):
    __gtype_name__ = 'NetworkInterfaceListboxRow'
    mainbox: Gtk.Box = Gtk.Template.Child()
    icon: Gtk.Image = Gtk.Template.Child()
    address_label: Gtk.Label = Gtk.Template.Child()
    ipv6_box: Gtk.Box = Gtk.Template.Child()
    macaddr_box: Gtk.Box = Gtk.Template.Child()
    inet6_address_label: Gtk.Label = Gtk.Template.Child()
    macaddr_label: Gtk.Label = Gtk.Template.Child()
    interface_label: Gtk.Label = Gtk.Template.Child()
    location_label: Gtk.Label = Gtk.Template.Child()
    copy_btn: Gtk.Button = Gtk.Template.Child()
    copy_ipv6_btn: Gtk.Button = Gtk.Template.Child()
    copy_macaddr_btn: Gtk.Button = Gtk.Template.Child()

    def __init__(self, net_iface: NetworkInterface):
        super().__init__()
        self.net_iface = net_iface

        self.net_iface.bind_property(
            'address', self.address_label, 'label',
            GObject.BindingFlags.SYNC_CREATE
        )

        self.net_iface.bind_property(
            'address_ipv6', self.inet6_address_label, 'label',
            GObject.BindingFlags.SYNC_CREATE
        )
        self.ipv6_box.set_visible(not not net_iface.address_ipv6)

        self.net_iface.bind_property(
            'mac_addr', self.macaddr_label, 'label',
            GObject.BindingFlags.SYNC_CREATE
        )
        self.macaddr_box.set_visible(not not self.net_iface.mac_addr)
        self.net_iface.connect('notify::interface', self.set_interface)
        self.set_interface()

        self.net_iface.bind_property(
            'geolocation', self.location_label, 'label',
            GObject.BindingFlags.SYNC_CREATE
        )
        self.location_label.set_visible(not not self.net_iface.geolocation)

    def set_interface(self, *__):
        self.interface_label.set_text(
            # network interface friendly name, followed by interface id
            _('{0}: {1}').format(
                NET_NAMES[self.net_iface.interface_type],
                self.net_iface.interface
            ) if self.net_iface.interface_type != NetType.INTERNET
            else NET_NAMES[self.net_iface.interface_type]
        )
        self.icon.set_from_icon_name(
            NET_ICONS[self.net_iface.interface_type]
        )
        self.icon.set_tooltip_text(NET_NAMES[
            self.net_iface.interface_type
        ])

    @Gtk.Template.Callback()
    def on_copy_ipv4_clicked(self, *args):
        clipboard_copy(self.net_iface.address)

    @Gtk.Template.Callback()
    def on_copy_ipv6_clicked(self, *args):
        clipboard_copy(self.net_iface.inet6_address)

    @Gtk.Template.Callback()
    def on_copy_macaddr_clicked(self, *args):
        clipboard_copy(self.net_iface.mac_addr)
