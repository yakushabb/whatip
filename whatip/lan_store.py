from typing import List, Optional
from gi.repository import Gtk, Gio, GObject
from whatip.lan_device import LanDevice
from whatip.lan_listbox_row import LanListboxRow
from whatip.network_interface import NetworkInterface
from whatip.network_interface_store import NetworkInterfaceStore
from whatip.ping import NetScanner


class LanStore(Gtk.SortListModel):
    __refreshing = False
    __progress: float = 0.0
    scanner: Optional[NetScanner]

    def __init__(self, listbox: Gtk.ListBox):
        self.listbox = listbox

        self.sorter = Gtk.CustomSorter()
        self.sorter.set_sort_func(self.__sort_func)
        self.list_store = Gio.ListStore(item_type=LanDevice)
        super().__init__(model=self.list_store, sorter=self.sorter)

        self.listbox.bind_model(self, self.__create_listbox_row, None)

    @GObject.Property(type=bool, default=False)
    def refreshing(self) -> bool:  # type: ignore
        return self.__refreshing

    @refreshing.setter
    def refreshing(self, nval: bool):  # type: ignore
        self.__refreshing = nval

    @GObject.Property(type=float)
    def progress(self) -> float:  # type: ignore
        return self.__progress

    @progress.setter
    def progress(self, nval: float):  # type: ignore
        self.__progress = nval

    def __create_listbox_row(self, item: LanDevice, *_) -> Gtk.ListBoxRow:
        row = LanListboxRow(item)
        return row

    def __sort_func(self, dev1: LanDevice, dev2: LanDevice, *_):
        return dev1.as_int() - dev2.as_int()

    def __on_dev_found_cb(self, dev_dict: Optional[dict]):
        if dev_dict is not None:
            dev = LanDevice(
                dev_dict['address'], dev_dict['hostname'], dev_dict['ports']
            )
            self.list_store.append(dev)

    def on_pool_finished(self, *_):
        self.refreshing = False

    def on_pool_progress(self, _, done: int, total: int):
        p = max(done, 0) / max(total, 1)
        self.progress = max(min(p, 1.0), 0.0)

    def populate(self, netIfaceStore: NetworkInterfaceStore):
        self.refreshing = True
        self.list_store.remove_all()
        targets: List[NetworkInterface] = [
            iface for iface in netIfaceStore.list_store if iface.has_lan()
        ]
        if len(targets) == 0:
            self.refreshing = False
            return
        target = targets[0]
        self.scanner = NetScanner(
            target.address, target.subnet_mask, self.__on_dev_found_cb
        )
        self.scanner.pool.connect('pool_finished', self.on_pool_finished)
        self.scanner.pool.connect('pool_progress', self.on_pool_progress)
        self.scanner.scan()
