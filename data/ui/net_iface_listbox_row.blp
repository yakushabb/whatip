using Gtk 4.0;

template NetworkInterfaceListboxRow : ListBoxRow {
    Box mainbox {
        orientation: vertical;
        spacing: 6;
        margin-start: 12; margin-end: 12; margin-top: 12; margin-bottom: 12;
        hexpand: true;
        Box {
            orientation: horizontal;
            spacing: 6;
            Image icon {icon-name: 'network-wired-symbolic'; }
            Label interface_label {
                styles ['heading']
                hexpand: true;
                xalign: 0.0;
            }
        }
        Box {
            orientation: horizontal;
            spacing: 6;
            Label address_label {
                styles ['title-2']
                tooltip-text: _('IPv4 Address');
                hexpand: true;
                xalign: 0.0;
                wrap: true; wrap-mode: char;
            }
            Button copy_btn {
                styles ['flat', 'circular']
                tooltip-text: _('Copy');
                halign: center;
                valign: center;
                icon-name: 'edit-copy-symbolic';
                clicked => on_copy_ipv4_clicked();
            }
        }
        Label location_label {
            tooltip-text: _('Location');
            hexpand: true;
            xalign: 0.0;
            wrap: true; wrap-mode: char;
        }
        Box ipv6_box {
            orientation: horizontal;
            Label inet6_address_label {
                tooltip-text: _('IPv6 Address');
                hexpand: true;
                xalign: 0.0;
                wrap: true; wrap-mode: char;
            }
            Button copy_ipv6_btn {
                styles ['flat', 'circular']
                tooltip-text: _('Copy');
                halign: center;
                valign: center;
                icon-name: 'edit-copy-symbolic';
                clicked => on_copy_ipv6_clicked();
            }
        }
        Box macaddr_box {
            orientation: horizontal;
            Box {
                tooltip-text: _('MAC Address');
                orientation: horizontal;
                hexpand: true;
                spacing: 6;
                Image {
                    icon-name: 'computer-chip-symbolic';
                    hexpand: false;
                }
                Label macaddr_label {
                    hexpand: true;
                    xalign: 0.0;
                    wrap: true; wrap-mode: char;
                }
            }
            Button copy_macaddr_btn {
                styles ['flat', 'circular']
                tooltip-text: _('Copy');
                halign: center;
                valign: center;
                icon-name: 'edit-copy-symbolic';
                clicked => on_copy_macaddr_clicked();
            }
        }
    }
}
