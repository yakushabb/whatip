using Gtk 4.0;
using Adw 1;

menu generalMenu {
    section {
        item (_("Preferences"), 'app.preferences')
        item (_("Keyboard Shortcuts"), 'app.shortcuts')
        item (_("About What IP"), 'app.about')
    }
}

template MainUI : Box {
    orientation: vertical;
    vexpand: true; hexpand: true;
    WindowHandle {
        vexpand: false;
        HeaderBar headerbar {
            show-title-buttons: true;
            [title] Adw.Squeezer squeezer {
                orientation: horizontal;
                homogeneous: false;
                interpolate-size: false;
                hexpand: false;
                notify::visible-child => on_squeeze();
                Adw.ViewSwitcher view_switcher {
                    margin-start: 12; margin-end: 12;
                    policy: wide;
                    stack: main_stack;
                }
                Label nobox {}
            }
            Button refresh_btn {
                tooltip-text: _("Refresh");
                icon-name: 'view-refresh-symbolic';
                clicked => on_refresh_btn_clicked();
            }
            [end] MenuButton menu_btn {
                tooltip-text: _("Menu");
                icon-name: 'open-menu-symbolic';
                menu-model: generalMenu;
            }
        }
    }
    Adw.ViewStack main_stack {
        vexpand: true;
        Adw.ViewStackPage ip_page {
            title: _("IP");
            icon-name: 'network-wired-symbolic';
            child: Stack ip_stack {
                StackPage {
                    child: Adw.StatusPage ip_empty {
                        title: _("No network interfaces connected");
                        icon-name: 'network-wired-disconnected-symbolic';
                    };
                }
                StackPage {
                    child: ScrolledWindow ip_view {
                        vexpand: true; hexpand: true;
                        hscrollbar-policy: never;
                        Adw.Clamp {
                            margin-start: 12; margin-end: 12;
                            margin-top: 32; margin-bottom: 32;
                            maximum-size: 600;
                            tightening-threshold: 360;
                            ListBox ip_listbox {
                                styles ['boxed-list']
                                valign: start;
                                selection-mode: none;
                                overflow: hidden;
                            }
                        }
                    };
                }
            };
        }
        Adw.ViewStackPage ports_page {
            title: _("Ports");
            icon-name: 'network-transmit-receive-symbolic';
            child: Stack ports_stack {
                StackPage {
                    child: Adw.StatusPage ports_empty {
                        title: _("No open ports detected");
                        icon-name: 'network-offline-symbolic';
                    };
                }
                StackPage {
                    child: ScrolledWindow ports_view {
                        vexpand: true; hexpand: true;
                        hscrollbar-policy: never;
                        Adw.Clamp {
                            margin-start: 12; margin-end: 12;
                            margin-top: 32; margin-bottom: 32;
                            maximum-size: 600;
                            tightening-threshold: 360;
                            ListBox ports_listbox {
                                styles ['boxed-list']
                                valign: start;
                                selection-mode: none;
                                overflow: hidden;
                            }
                        }
                    };
                }
            };
        }
        Adw.ViewStackPage lan_page {
            title: _("LAN");
            icon-name: 'preferences-system-network-symbolic';
            child: Stack lan_stack {
                StackPage {
                    child: Adw.StatusPage lan_empty {
                        title: _("Scan for devices on your local network");
                        icon-name: 'preferences-system-network-symbolic';
                        Button lan_scan_btn {
                            styles ['pill', 'suggested-action']
                            label: _("Scan");
                            hexpand: false;
                            halign: center;
                            clicked => on_lan_scan_btn_clicked();
                        }
                    };
                }
                StackPage {
                    child: Overlay lan_view {
                        vexpand: true; hexpand: true;
                        [overlay] Revealer lan_progress_revealer {
                            valign: start; vexpand: false;
                            ProgressBar lan_progressbar {
                                styles ['osd']
                            }
                        }
                        ScrolledWindow {
                            vexpand: true; hexpand: true;
                            hscrollbar-policy: never;
                            Box {
                                orientation: vertical;
                                spacing: 12;
                                Button lan_rescan_btn {
                                  styles ['pill', 'suggested-action']
                                  label: _("Scan");
                                  hexpand: false;
                                  halign: center;
                                  margin-top: 24;
                                  clicked => on_lan_scan_btn_clicked();
                                }
                                Adw.Clamp {
                                    margin-start: 12; margin-end: 12;
                                    margin-top: 12; margin-bottom: 32;
                                    maximum-size: 600;
                                    tightening-threshold: 360;
                                    ListBox lan_listbox {
                                        styles ['boxed-list']
                                        valign: start;
                                        selection-mode: none;
                                        overflow: hidden;
                                    }
                                }
                            }
                        }
                    };
                }
            };
        }
    }
    Adw.ViewSwitcherBar bottom_bar {
        stack: main_stack;
        reveal: true;
    }
}
